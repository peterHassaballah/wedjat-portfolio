import React from 'react';
import logo from './img/back.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo}  alt="logo" />
        
        <h1>Wedjat Studio</h1>
        <h2>Coming soon</h2>
      </header>
    </div>
  );
}

export default App;
